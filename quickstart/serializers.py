from .models import Book, Bookshelf
from rest_framework import serializers

class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Book
        fields = ['bookshelf_name', 'name', 'fio', 'shortcut', 'status']

class BookShelfSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bookshelf
        fields = ['name']