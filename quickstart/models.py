from django.db import models

# Create your models here.

class Bookshelf(models.Model):
    name = models.CharField(max_length=50, primary_key=True)

    def __str__(self):
        return self.name

class Book(models.Model):
    bookshelf_name = models.CharField(max_length=50)
    name = models.CharField(max_length=30)
    fio = models.CharField(max_length=30)
    shortcut = models.CharField(max_length=2000)
    status = models.BooleanField()