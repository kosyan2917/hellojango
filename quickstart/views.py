from rest_framework import viewsets
from rest_framework import permissions
from quickstart.serializers import BookSerializer, BookShelfSerializer
from .models import Book, Bookshelf


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = [permissions.AllowAny]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Bookshelf.objects.all()
    serializer_class = BookShelfSerializer
    permission_classes = [permissions.AllowAny]